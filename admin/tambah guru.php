<<<<<<< HEAD
<html>
  <head>
    <title>Admin SMP Negeri 1 Cikijing</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link rel="stylesheet" href="css/material.min.css" media="screen,projection">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/material.css">    
    <link rel="stylesheet" href="css/materialDate.css">         
    <link href="css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
    
    <script src="libs/moment.min.js"></script>
    <script src="libs/jquery-2.1.3.min.js"></script>
    <script src="libs/knockout-3.2.0.js"></script>
    <script src="material-datepicker/js/material.datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="material-datepicker/css/material.datepicker.css">
 
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
  </head>
  <body>
	<?php
		
		session_start();
		include "koneksi.php";
		/*
		if(!isset($_session['id'])){
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php">';	
		}*/
		
		
		$admin_name = $_SESSION["user_name"];
		$admin_foto = $_SESSION["user_foto"];
		
		
	?>
	        
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/material.min.js"></script>  
    <script src="js/materialDate.js"></script>  
	<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
<header class="demo-header mdl-layout__header mdl-color--white mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">Home</span>
          <div class="mdl-layout-spacer"></div>
          <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
            <i class="material-icons"><i class="mdi mdi-dots-vertical"></i></i>
          </button>
          <ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
            <a href="logout.php"><li class="mdl-menu__item">Log Out</li></a>
          </ul>
        </div>
      </header>
       <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <header class="demo-drawer-header">
          <img src="images/guru/<?php echo $admin_foto; ?>" class="demo-avatar">
          <div class="demo-avatar-dropdown">
            <span><?php echo $admin_name ;?></span>
            <div class="mdl-layout-spacer"></div>
            <button id="accbtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
              <i class="material-icons" role="presentation"><i class="mdi mdi-menu-down"></i></i>
            </button>
            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="accbtn">
              <a href="setting.php"><li class="mdl-menu__item"><i class="mdi mdi-settings"></i>  Setting</li></a>            
            </ul>                        
          </div>
		</header>
        <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="admin.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation"><i class="mdi mdi-bank"></i></i>Home</a>                
          <a class="mdl-navigation__link" href="tambah guru.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation"><i class="mdi mdi-account-plus"></i></i>Tambah Guru</a>
          <a class="mdl-navigation__link" href="lihat guru.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation"><i class="mdi mdi-account"></i></i>Lihat Guru</a>
          <a class="mdl-navigation__link" href="tambah kelas.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation"><i class="mdi mdi-home-variant"></i></i>Tambah Kelas</a>
          <a class="mdl-navigation__link" href="lihat kelas.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation"><i class="mdi mdi-home"></i></i>Lihat Kelas</a>                    
          <a class="mdl-navigation__link" href="tambah murid.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation"><i class="mdi mdi-account-multiple-plus"></i></i>Tambah Murid</a>
          <a class="mdl-navigation__link" href="lihat murid.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation"><i class="mdi mdi-account-multiple"></i></i>Lihat Murid</a>                    
        </nav>
      </div>
	 <main class="mdl-layout__content mdl-color--white-100">
        <div class="mdl-grid demo-content">
          <div class="demo-charts mdl-color--white  mdl-cell mdl-cell--12-col mdl-grid">
            <!-- Form Tambah Guru-->
                <form role="form" action="proses tambah guru.php" method="post" name="postform" enctype="multipart/form-data">
                <h4>FORM TAMBAH GURU</h4>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label for="nip" class="mdl-textfield__label">NIP</label>
                    <input type="text" pattern="[P0-9]*" class="mdl-textfield__input" id="nip" name="nip" />
                    <span class="mdl-textfield__error">Format : PXXX</span>
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label for="namadepan" class="mdl-textfield__label">Nama Depan</label>
                    <input type="text" pattern="[A-Za-z]*" class="mdl-textfield__input" id="namadepan" name="namadepan" />
                    <span class="mdl-textfield__error">Tidak boleh berupa angka atau simbol</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label for="namabelakang" class="mdl-textfield__label">Nama Belakang</label>
                    <input type="text" pattern="[A-Za-z]*" class="mdl-textfield__input" id="namabelakang" name="namabelakang" />
                    <span class="mdl-textfield__error">Tidak boleh berupa angka atau simbol</span>
                </div>                
                <br>
                <label for="option-1" class="mdl-radio mdl-js-radio mdl-js-ripple-effect">
                    <input type="radio" name="jk" value="Pria" class="mdl-radio__button" id="option-1" checked/>
                    <span class="mdl-radio__label"> Pria </span>
                </label>

                <label for="option-2" class="mdl-radio mdl-js-radio mdl-js-ripple-effect">
                    <input type="radio" name="jk" value="Wanita" class="mdl-radio__button" id="option-2" />
                    <span class="mdl-radio__label"> Wanita</span>
                </label>
                <br>
                 <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
                    <label >Tanggal Lahir</label>
                    <input type="date" class="mdl-textfield__input" id="tanggallahir" name="tanggallahir"/>                    
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label for="email" class="mdl-textfield__label">Email</label>
                    <input type="email" class="mdl-textfield__input" id="email" name="email" />
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
                    <label for="username" class="mdl-textfield__label">Username</label>
                    <input type="text" class="mdl-textfield__input" id="username" name="username" />
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label for="password" class="mdl-textfield__label">Password</label>
                    <input type="password"  class="mdl-textfield__input" id="password" name="password" />
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label for="telepon" class="mdl-textfield__label">No Telp</label>
                    <input type="text" pattern="[0-9]*" class="mdl-textfield__input" id="telepon" name="telepon" />
                    <span class="mdl-textfield__error">Masukan hanya berupa angka</span>
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label >Masukan Foto (JPG atau PNG)</label>
                    <input type="file" class="mdl-textfield__input" name="file" id="file" value="Masukan Foto PNG atau JPEG" />
                </div>
                <br>                
                <label for="checkbox-1" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" >
                    <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input" name="checkGuru" />
                    <span class="mdl-checkbox__label">Menyatakan bahwa saya benar seorang guru</span>
                </label>

                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " type="submit">Tambah Guru</button>

            </form>

            <!-- /form tambah guru-->
          </div>
        </div>
      </main>
    </div>



  </body>
</html>
=======
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard Admin - SMP Negeri 1 Cikijing</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="plugins/kalendar/kalendar.css" rel="stylesheet">
<link rel="stylesheet" href="plugins/scroll/nanoscroller.css">
<link href="plugins/morris/morris.css" rel="stylesheet" />
</head>
<body class="light_theme  fixed_header left_nav_fixed">
<?php
	
	session_start();
	include "koneksi.php";
	/*
	if(!isset($_session['id'])){
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php">';	
	}*/
	
	
	$admin_name = $_SESSION["user_name"];
	$admin_foto = $_SESSION["user_foto"];
	
	
?>
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>

<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
  <div class="header_bar">
    <!--\\\\\\\ header Start \\\\\\-->
    <div class="brand">
      <!--\\\\\\\ brand Start \\\\\\-->
      <div class="logo" style="display:block"><span class="theme_color">ADMIN</span> Dashboard</div>
      <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="images/r-logo.png" width="122" height="20" alt="r-logo" /></div>
    </div>
    <!--\\\\\\\ brand end \\\\\\-->
    <div class="header_top_bar">
      <!--\\\\\\\ header top bar start \\\\\\-->
      <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
      <div class="top_right_bar">
        <div class="top_right">
          <div class="top_right_menu">
            <ul>
              </ul>
          </div>
        </div>
        <div class="user_admin dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"><img class="img-circle" width="50" height= "50" src="images/profil/<?php echo $admin_foto;?>" /><span class="user_adminname"><?php echo $admin_name;?></span> <b class="caret"></b> </a>
          <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <li> <a href="profile.html"><i class="fa fa-user"></i> Profile</a> </li>
            <li> <a href="help.html"><i class="fa fa-question-circle"></i> Help</a> </li>
            <li> <a href="logout.php"><i class="fa fa-power-off"></i> Logout</a> </li>
          </ul>
        </div>

        
      </div>
    </div>
    <!--\\\\\\\ header top bar end \\\\\\-->
  </div>
  <!--\\\\\\\ header end \\\\\\-->
  <div class="inner">
    <!--\\\\\\\ inner start \\\\\\-->
    <div class="left_nav">

      <!--\\\\\\\left_nav start \\\\\\-->
      <div class="left_nav_slidebar">
        <ul>
        <!-- Menu Dashboard -->
          <li ><a href="javascript:void(0);"><i class="fa fa-home"></i> DASHBOARD <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul >
              <li> <a href="index.html"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Dashboard</b> </a> </li>
              <li> <a href="settings.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Settings</b> </a> </li>
              <li> <a href="layouts.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Layouts</b> </a> </li>
              <li> <a href="themes.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Themes</b> </a> </li>
              <li> <a href="widgets.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Widgets</b> </a> </li>
              <li> <a href="animations.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Animations</b> </a> </li>
            </ul>
          </li>
          <!-- Menu Guru -->
          <li class="left_nav_active theme_border"> <a href="javascript:void(0);"> <i class="fa fa-edit"></i> Guru <span class="plus"><i class="fa fa-plus"></i></span></a>
            <ul class="opened" style="display:block">
              <li> <a href="lihat guru.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Lihat Guru</b> </a> </li>
              <li> <a href="tambah guru.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Tambah Guru</b> </a> </li>
            </ul>
          </li>
          <!-- Menu Murid -->
          <li> <a href="javascript:void(0);"> <i class="fa fa-tasks"></i> Murid <span class="plus"><i class="fa fa-plus"></i></span></a>
            <ul>
              <li> <a href="lihat murid.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Lihat Murid</b> </a> </li>
              <li> <a href="tambah murid.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Tambah Murid</b> </a> </li>
            </ul>
          </li>
          <!-- Kelas -->
          <li> <a href="javascript:void(0);"> <i class="fa fa-users icon"></i> Kelas <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul>
            <li> <a href="lihat kelas.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Lihat Kelas</b> </a> </li>
              <li> <a href="tambah kelas.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Tambah Kelas</b> </a> </li>         
            </ul>
          </li>
          </ul>
      </div>
    </div>
    <!--\\\\\\\left_nav end \\\\\\-->
    <div class="contentpanel">
      <!--\\\\\\\ contentpanel start\\\\\\-->
      <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Tambah Guru</h1>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Guru</a></li>
            <li class="active">Tambah Guru</li>
          </ol>
        </div>               
      </div>

<div class="container clear_both padding_fix">
 <div class="porlets-content">
   <div class="col-md-6">
      <div class="block-web">
        <!--\\\\\\\ container  start \\\\\\-->
      <h1>Form Data Guru</h1>
      <form role="form" class="col s12" action="proses tambah guru.php" method="post" name="postform" enctype="multipart/form-data">

	    <div class="row">  
          NIP
            <input id="nip" name="nip" type="text" placeholder="Username" class="validate">          
        </div>
		<div class="row">  
          <div class="col s6">
          Nama Lengkap
            <input id="namaDepan" name="namadepan" type="text" placeholder="Nama Depan" class="validate">
          </div>
          <div class="col s6">
          <br>
            <input id="namabelakang" name="namabelakang" type="text" placeholder="Nama Belakang" class="validate">
          </div>
        </div>
        <div class="row">
          Jenis Kelamin 
          <p>
            <input name="jk" type="radio" id="test1" value="Pria" />
            <label for="test1">Pria</label> 
            <input name="jk" type="radio" id="test2" value="Wanita"/>
            <label for="test2">Wanita</label>
          </p>
        </div>  
        <div class="row">
              Tanggal Lahir
              <input type="date" class="datepicker" name="tanggallahir" >
        </div>       
        <div class="row">
            Email
            <input id="email" name="email" type="email" placeholder="sample@gmail.com" class="validate">          
        </div>
        <div class="row">  
          Username
            <input id="username" name="username" type="text" placeholder="Username" class="validate">          
        </div>

        <div class="row">
                  Password
              <input id="password" name="password" type="password" placeholder="Masukan password" class="validate">
        </div>
        <div class="row">  
         Telepon
            <input id="telepon" name="telepon" type="text" placeholder="85xxxxxxxxxx" class="validate">          
        </div>
        <div class="row">
              <input type="file" name="file" id="file">
           </div>

        <div class="row">
              <button class="btn btn-default pull-right" type="submit">Tambah</button>
        </div>
       
      </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
  <!--\\\\\\\ inner end\\\\\\-->
</div>
<!--\\\\\\\ wrapper end\\\\\\-->

<script src="js/jquery-2.1.0.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/common-script.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<script src="js/jquery.sparkline.js"></script>
<script src="js/sparkline-chart.js"></script> 
<script src="js/graph.js"></script>
<script src="js/edit-graph.js"></script>
<script src="plugins/kalendar/kalendar.js" type="text/javascript"></script>
<script src="plugins/kalendar/edit-kalendar.js" type="text/javascript"></script>

<script src="plugins/sparkline/jquery.sparkline.js" type="text/javascript"></script>
<script src="plugins/sparkline/jquery.customSelect.min.js" ></script> 
<script src="plugins/sparkline/sparkline-chart.js"></script> 
<script src="plugins/sparkline/easy-pie-chart.js"></script>
<script src="plugins/morris/morris.min.js" type="text/javascript"></script> 
<script src="plugins/morris/raphael-min.js" type="text/javascript"></script>  
<script src="plugins/morris/morris-script.js"></script> 





<script src="plugins/demo-slider/demo-slider.js"></script>
<script src="plugins/knob/jquery.knob.min.js"></script> 




<script src="js/jPushMenu.js"></script> 
<script src="js/side-chats.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<script src="plugins/scroll/jquery.nanoscroller.js"></script>
<script>
   $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });
</script>

</body>
</html>
>>>>>>> a2fc5e42853c8c238768ec5848bace74846789d2
