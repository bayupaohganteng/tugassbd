<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard Admin - SMP Negeri 1 Cikijing</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="plugins/kalendar/kalendar.css" rel="stylesheet">
<link rel="stylesheet" href="plugins/scroll/nanoscroller.css">
<link href="plugins/morris/morris.css" rel="stylesheet" />
</head>
<body class="light_theme  fixed_header left_nav_fixed">
<?php
  
  session_start();
  include "koneksi.php";
  /*
  if(!isset($_session['id'])){
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php">';  
  }*/   
  $admin_name = $_SESSION["user_name"];
  $admin_foto = $_SESSION["user_foto"];
  $nim=$_GET['id'];
    
?>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
  <div class="header_bar">
    <!--\\\\\\\ header Start \\\\\\-->
    <div class="brand">
      <!--\\\\\\\ brand Start \\\\\\-->
      <div class="logo" style="display:block"><span class="theme_color">ADMIN</span> Dashborad</div>
      <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="images/r-logo.png" width="122" height="20" alt="r-logo" /></div>
    </div>
    <!--\\\\\\\ brand end \\\\\\-->
    <div class="header_top_bar">
      <!--\\\\\\\ header top bar start \\\\\\-->
      <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
            <div class="top_right_bar">
        <div class="top_right">
          <div class="top_right_menu">
            <ul>
              </ul>
          </div>
        </div>
        <div class="user_admin dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"><img src="images/profil/<?php echo $admin_foto; ?>" class="img-circle" width="50" height= "50"><span class="user_adminname"><?php echo $admin_name;?></span> <b class="caret"></b> </a>
          <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <li> <a href="profile.html"><i class="fa fa-user"></i> Profile</a> </li>
            <li> <a href="help.html"><i class="fa fa-question-circle"></i> Help</a> </li>
            <li> <a href="logout.php"><i class="fa fa-power-off"></i> Logout</a> </li>
          </ul>
        </div>

        
      </div>
    </div>
    <!--\\\\\\\ header top bar end \\\\\\-->
  </div>
  <!--\\\\\\\ header end \\\\\\-->
  <div class="inner">
    <!--\\\\\\\ inner start \\\\\\--><div class="left_nav">

      <!--\\\\\\\left_nav start \\\\\\-->
      <div class="left_nav_slidebar">
        <ul>
        <!-- Menu Dashboard -->
          <li ><a href="javascript:void(0);"><i class="fa fa-home"></i> DASHBOARD <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul >
              <li> <a href="index.html"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Dashboard</b> </a> </li>
              <li> <a href="settings.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Settings</b> </a> </li>
              <li> <a href="layouts.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Layouts</b> </a> </li>
              <li> <a href="themes.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Themes</b> </a> </li>
              <li> <a href="widgets.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Widgets</b> </a> </li>
              <li> <a href="animations.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Animations</b> </a> </li>
            </ul>
          </li>
          <!-- Menu Guru -->
          <li class="left_nav_active theme_border"> <a href="javascript:void(0);"> <i class="fa fa-edit"></i> Guru <span class="plus"><i class="fa fa-plus"></i></span></a>
            <ul class="opened" style="display:block">
              <li> <a href="lihat guru.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Lihat Guru</b> </a> </li>
              <li> <a href="tambah guru.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Tambah Guru</b> </a> </li>
            </ul>
          </li>
          <!-- Menu Murid -->
          <li> <a href="javascript:void(0);"> <i class="fa fa-tasks"></i> Murid <span class="plus"><i class="fa fa-plus"></i></span></a>
            <ul>
              <li> <a href="lihat murid.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Lihat Murid</b> </a> </li>
              <li> <a href="tambah murid.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Tambah Murid</b> </a> </li>
            </ul>
          </li>
          <!-- Kelas -->
          <li> <a href="javascript:void(0);"> <i class="fa fa-users icon"></i> Kelas <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul>
            <li> <a href="lihat kelas.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Lihat Kelas</b> </a> </li>
              <li> <a href="tambah kelas.php"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Tambah Kelas</b> </a> </li>         
            </ul>
          </li>
          </ul>
      </div>
    </div>
  
  <?php
   $query=mysql_query("select * from siswa where id_siswa='$nim'");
   while ($data=mysql_fetch_array($query)){     
      $nim=$data["id_siswa"];
      $nama=$data["nama_siswa"];
      $jk=$data["jk"];
      $tgl_lahir=$data["tgl_lahir"];
      $alamat=$data["alamat"];
      $nama_ortu=$data["nama_ortu"];
      $telp_ortu=$data["telp_ortu"];
      $no_telp=$data["no_telp"];
      $foto_murid=$data["foto_murid"];
    
  } 
  ?>
    <!--\\\\\\\left_nav end \\\\\\-->
    <div class="contentpanel">
      <!--\\\\\\\ contentpanel start\\\\\\-->
      <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1><?php echo $nim; ?></h1>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Guru</a></li>
            <li class="active">Lihat Guru</li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        <div class="row">
          <div class="col-sm-9">
            <div class="row">
            <!--/col-md-4-->
            <div class="col-md-8">
              <div class="block-web full">
                <ul class="nav nav-tabs nav-justified nav_bg">
                  <li class="active"><a href="#about" data-toggle="tab"><i class="fa fa-user"></i> About</a></li>
                  <li class=""><a href="#edit-profile" data-toggle="tab"><i class="fa fa-pencil"></i> Edit</a></li>
          <li class=""><a href="proses hapus murid.php?id=<?php echo $nim;?>" ><i class="fa fa-pencil"></i> Hapus</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane animated fadeInRight active" id="about">
                    <div class="user-profile-content">
                      <p align="center">
                      <img src="images/photomurid/<?php echo $foto_profil; ?>" height="100" width="100" class="img-circle" />
                      </p>
                      <hr>
                      <div class="row">
                        <div class="col-sm-6">
                          <h5><strong>INFO</strong> KONTAK</h5>
                          <address>
                          <strong>Phone</strong><br>
                          <abbr title="Phone">+62<?php echo $no_telp;?></abbr>
                          </address>
                          <address>
                          <strong>Nama Ortu : </strong><br>
                          <a href="#"><?php echo $nama_ortu;?></a>
                          </address>
                          <address>
                          <strong>Telp Ortu : </strong><br>
                          <a href="#"><?php echo $telp_ortu;?></a>
                          </address>              
                          </div>
                        <div class="col-sm-6">
                          <h5><strong>INFO</strong> PERSONAL</h5>
                          <p>NIM     : <?php echo $nim;?></p>
                          <p>Nama `  : <?php echo $nama;?></p>
                          <p>JK      : <?php echo $jk;?></p>              
                          <p>TTL     : <?php echo $tgl_lahir;?></p>
                          <p>Alamat  : <?php echo $alamat;?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane animated fadeInRight" id="edit-profile">
                    <div class="user-profile-content67` ">
                      <form role="form" class="col s12" action="proses tambah murid.php" method="post" name="postform" enctype="multipart/form-data">
        <div class="row">  
          NIM
            <input id="nip" name="nip" type="text" class="validate" value="<?php echo $nim; ?>" readonly>          
        </div>
        <div class="row">  
            Nama Lengkap
            <input id="nama" name="nama" type="text" value="<?php  echo $nama;?>" class="validate">
        </div>
        <div class="row">
        Jenis Kelamin 
          <p>
            <input name="jk" type="radio" id="test1" value="Pria" <?php if($jk == "Pria"){echo 'checked';}else{ echo '';}?>/>
            <label for="test1">Pria</label> 
            <input name="jk" type="radio" id="test2" value="Wanita" <?php if($jk != "Pria"){echo 'checked';}else{ echo '';}?>/>
            <label for="test2">Wanita</label>
          </p>
        </div>  
        <div class="row">
            Tanggal Lahir
            <input type="date" class="datepicker" name="tanggallahir" value="<?php echo $tgl_lahir; ?>">
        </div>
        <div class="row">
            Alamat
            <textarea name="alamat" id="textarea1" class="materialize-textarea" value="<?php echo $alamat; ?>"></textarea>
        </div> 
        <div class="row">  
            No Telp (+62)
            <input id="no_telp" name="no_telp" type="text" placeholder="8xxxxxxxxxx" value="<?php echo $no_telp; ?>" class="validate">          
        </div>

        <div class="row">  
            Nama Orang Tua
            <input id="nama_ortu" name="nama_ortu" type="text" value="<?php echo $nama_ortu; ?>" class="validate">          
        </div>

        <div class="row">
            Telepon Orang Tua (+62)
            <input id="telp_ortu" name="telp_ortu" type="text" value="<?php echo $telp_ortu; ?>" placeholder="Masukan password" class="validate">
        </div>
        <div class="row">
            Kelas            
            <select class="browser-default" name="kelas">
              <option value="" disabled selected>Pilih kelas</option>
            <?php
              $query1="select * from kelas";
              while ($data=mysql_query($query1)) {
                $id_kelas=$data["id_kelas"];
                $nama = $data["nama_kelas"];
            ?>
              <option value="<?php echo $id_kelas; ?>"><?php echo $nama; ?></option>
            </select>
            <?php }?>
        </div>
        
        <div class="row">
            Masukkan foto siswa (file yang diterima jpg & png):
            <input type="file" name="file" id="file">
        </div>

        <div class="row">          
              <button class="btn btn-default pull-right" type="submit">Edit</button>
        </div>
       
      </form>

     </div>
                  </div>
                  </div>
                <!--/tab-content-->
              </div>
              <!--/block-web-->
            </div>
            <!--/col-md-8-->

            </div>          
      
          </div>
          </div>
          
          <div class="row">
        <div class="col s12 m7">

        </div>
      </div>

        
          <!--/col-md-12-->
        </div>
        <!--/row-->
        </div>
        
        
        
     <div class="row">
        </div>   
        
        
        <div class="row">
          <!--/col-md-4 end-->
        </div>
        <!--/row end-->
        
         <!--row start-->
        <div class="row">        
          
        </div>
        <!--row end--> 
 
        
      </div>
      <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
  </div>
  <!--\\\\\\\ inner end\\\\\\-->
</div>
<!--\\\\\\\ wrapper end\\\\\\-->
<!-- Modal -->


<!-- /sidebar chats -->   














<!-- sidebar chats -->
<!-- /sidebar chats -->   









<script src="js/jquery-2.1.0.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/common-script.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<script src="js/jquery.sparkline.js"></script>
<script src="js/sparkline-chart.js"></script>
<script src="js/graph.js"></script>
<script src="js/edit-graph.js"></script>
<script src="plugins/kalendar/kalendar.js" type="text/javascript"></script>
<script src="plugins/kalendar/edit-kalendar.js" type="text/javascript"></script>

<script src="plugins/sparkline/jquery.sparkline.js" type="text/javascript"></script>
<script src="plugins/sparkline/jquery.customSelect.min.js" ></script> 
<script src="plugins/sparkline/sparkline-chart.js"></script> 
<script src="plugins/sparkline/easy-pie-chart.js"></script>
<script src="plugins/morris/morris.min.js" type="text/javascript"></script> 
<script src="plugins/morris/raphael-min.js" type="text/javascript"></script>  
<script src="plugins/morris/morris-script.js"></script> 





<script src="plugins/demo-slider/demo-slider.js"></script>
<script src="plugins/knob/jquery.knob.min.js"></script> 




<script src="js/jPushMenu.js"></script> 
<script src="js/side-chats.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<script src="plugins/scroll/jquery.nanoscroller.js"></script>



</body>
</html>
