/*Develop by Bayu Firmawan Paoh - SBD 5 - 10113076*/
drop database if exists sbd_10113076;
create database sbd_10113076;
use sbd_10113076;

create table sekolah (
	id_sekolah int primary key,
	nama_sekolah varchar(30),
	alamat varchar(30),
	deskripsi text
);

insert into sekolah values(1,'SMP Negeri 1 Cikijing','Jl. Cikijing Raya Majalengka 12','Sekolah yang fokus akan teknologi informasi');

create table guru (
	id_guru varchar(8) primary key,
	nama_guru varchar(20),
	jk enum('Pria','Wanita'),
	tgl_lahir date,
	email varchar(21) not null,	
	username varchar(16) unique not null,
	password varchar(50) not null,
	no_telp varchar(13),
	foto_profil varchar(20),
	status enum('Admin','Guru')	
);

insert into guru values('P1','Bayu Firmawan Paoh','Pria','1994-05-17','bayupaoh@gmail.com','bayupaoh','123456','85738812545','bayu.png','Admin');
insert into guru values('P2','Samsul Arifin','Pria','1994-05-17','samsularifin@gmail.com','samsularifin','123456','85738812545','samsularifin.png','Guru');
insert into guru values('P3','Diyang Mardiana','Pria','1994-05-17','diyangmardiana@gmail.com','diyang','123456','85738812545','samsularifin.png','Guru');

create table kelas(
	id_kelas varchar(8) primary key,
	nama_kelas varchar(20),
	id_sekolah int,
	id_guru varchar(8),
	foreign key(id_sekolah) references sekolah(id_sekolah),
	foreign key(id_guru) references guru(id_guru)		
);

insert into kelas values('K1','VII-A','1','P2');
insert into kelas values('K2','VII-B','1','P3');
insert into kelas values('K3','VIII-B','1','P2');

create table siswa(
	id_siswa varchar(8) primary key,
	nama_siswa varchar(20),
	jk enum('Pria','Wanita'),
	tgl_lahir date,
	alamat varchar(30),
	no_telp varchar(13),
	nama_ortu varchar(20),
	telp_ortu varchar(13),
	foto_murid varchar(31),
	id_kelas varchar(8),
	foreign key(id_kelas) references kelas(id_kelas)
);

insert into siswa values('10115001','Bayu Tampan Sekali','Pria','1994-05-17','Cikijing','85738812545','Asep','85237373337','murid.png','K2');

create table absensi(
	id_absensi int primary key auto_increment,
	tanggal date,
	keterangan enum('Sakit','Izin','Alpa','Hadir'),
	id_kelas varchar(8),
	id_siswa varchar(8),
	foreign key(id_kelas) references kelas(id_kelas),
	foreign key(id_siswa) references siswa(id_siswa)
);